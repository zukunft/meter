/*
 * @Author: zengzeping
 * @Date: 2019-07-16 11:03:36
 * @LastEditors: zengzeping
 * @LastEditTime: 2019-09-06 17:15:05
 * @Description: file content
 */
import Mock from 'mockjs'
// const Random = Mock.Random

// const data=Mock.mock()

// 模拟数据
Mock.mock('/api/meter', 'get', {
    'meter|15': [{
        name: '@cname',
        "val|-10--120": -80,
        min: 0,
        max: -120,
        unit: 'Kpa'
    }]
})
Mock.mock('/api/station', 'get', {
    'station|15': [{
        name: '@cname',
        "status|-1-2": 0,
        'sum|10-200': 20,
        'NG|2-10': 2,
        color: "@rgb"
    }]
})