/*
 * @Author: zengzeping
 * @Date: 2019-09-05 10:02:07
 * @LastEditors: zengzeping
 * @LastEditTime: 2019-09-06 16:47:23
 * @Description: file content
 */
import Vue from 'vue'
import App from './App.vue'
import router from './router'
import axios from 'axios'
//mockjs
import "./mock.js"
Vue.prototype.$axios = axios;
Vue.config.productionTip = false

new Vue({
    router,
    render: h => h(App)
}).$mount('#app')