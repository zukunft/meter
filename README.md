<!--
 * @Author: zengzeping
 * @Date: 2019-09-06 17:18:34
 * @LastEditors: zengzeping
 * @LastEditTime: 2019-09-06 17:19:25
 * @Description: file content
 -->

# Meter

## Project setup

```
npm install
```

### Compiles and hot-reloads for development

```
npm run serve
```

### Compiles and minifies for production

```
npm run build
```

### Run your tests

```
npm run test
```

### Lints and fixes files

```
npm run lint
```

### Customize configuration

See [Configuration Reference](https://cli.vuejs.org/config/).
